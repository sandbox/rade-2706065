<?php

/**
 * @file
 * Hooks provided by the Suggest module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter a suggestable text before showing it to the user.
 *
 * This hook is useful for stripping tags, shortcodes, node embeds and such
 * from suggestable text before showing it to the user.
 *
 * @param string $text
 *   The text that the user will be able to edit to make suggestions.
 */
function hook_suggest_text_alter(&$text) {
  // Strip all node embeds from the text.
  preg_replace('/\[\[nid:(\d+)(\s.*)?\]\]/U', '', $text);
}

/**
 * @} End of "addtogroup hooks".
 */
