<?php

/**
 * @file
 * Page callbacks for the Suggest module.
 */

/**
 * Form callback for suggestion jump menu.
 */
function suggest_suggestions_jump_menu($form, &$form_state, $nid) {
  $select = suggest_get_suggestion_list($nid);
  ctools_include('jump-menu');
  $options = array(
    'choose' => t('- Select contribution -'),
  );
  $form = ctools_jump_menu($form, $form_state, $select, $options);
  return $form;
}

/**
 * Page callback for viewing a suggestion.
 */
function suggest_suggestion_view($node, $suggestion_id = NULL) {
  $jump_menu = drupal_get_form('suggest_suggestions_jump_menu', $node->nid);

  if (!$suggestion_id) {
    return $jump_menu;
  }

  if (!$suggestion = suggest_load($suggestion_id)) {
    return MENU_NOT_FOUND;
  }

  if ($suggestion['entity_type'] !== 'node' || $suggestion['entity_id'] != $node->nid) {
    return MENU_ACCESS_DENIED;
  }

  // Make sure the diff js library is installed.
  $library = libraries_detect('prettytextdiff');
  if (!$library['installed']) {
    drupal_set_message(t('The @name library could not be found. Please <a href="@url">download</a> it and place it in <em>sites/all/libraries/prettytextdiff</em>.', array(
      '@name' => $library['name'],
      '@url' => $library['download url'],
    )), 'warning');
  }

  $page = array();

  foreach ($suggestion['fields'] as $field_name => $field_data) {
    $page[] = array(
      '#type' => 'item',
      '#title' => t('Suggested !field_name', array('!field_name' => $field_name)),
      '#markup' => '
      <div class="' . drupal_clean_css_identifier($field_name) . '-diff suggest-diff">
        <div class="original">' . check_plain($field_data['original']) . '</div>
        <div class="changed">' . check_plain($field_data['suggestion']) . '</div>
        <div class="diff"></div>
      </div>
      ',
    );
  }

  if (!empty($suggestion['user']['name'])) {
    $page[] = array(
      '#type' => 'item',
      '#title' => t('User name'),
      '#markup' => check_plain($suggestion['user']['name']),
    );
  }
  if (!empty($suggestion['user']['contact'])) {
    $page[] = array(
      '#type' => 'item',
      '#title' => t('User contact info'),
      '#markup' => check_plain($suggestion['user']['contact']),
    );
  }
  $page['#attached'] = array(
    'css' => array(
      drupal_get_path('module', 'suggest') . '/css/suggest.css',
    ),
    'libraries_load' => array(
      array('prettytextdiff'),
    ),
    'js' => array(
      'https://cdnjs.cloudflare.com/ajax/libs/diff_match_patch/20121119/diff_match_patch.js',
      drupal_get_path('module', 'suggest') . '/js/suggest.js',
    ),
  );

  $output = array(
    'jump_menu' => $jump_menu,
    'suggestion' => array(
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => t('Contribution created at @date', array('@date' => format_date($suggestion['created']))),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#attributes' => array(
        'class' => array('suggest'),
      ),
      'page' => $page,
    ),
  );
  return $output;
}
