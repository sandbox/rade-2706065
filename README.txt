INTRODUCTION
------------

Suggest is a module that allows a visitor (anonymous user) to send a
contribution to an already published node. Site editors are able to see the
contributions on the individual node. They can see a diff between the original
content and the submitted suggestion.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/suggest

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/suggest


REQUIREMENTS
------------

This module requires the following modules:

 * Chaos tools suite (https://drupal.org/project/ctools)
 * Entity API (https://drupal.org/project/entity)
 * Libraries API (https://drupal.org/project/libraries)
 * jQuery.PrettyTextDiff (https://github.com/arnab/jQuery.PrettyTextDiff)


RECOMMENDED MODULES
-------------------

 * Honeypot (https://www.drupal.org/project/honeypot):
   When enabled, protects the contribution forms from spam-bots.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

   - Adminster Suggest

     Users in roles with the "Administer Suggest" permission will be able to
     access the module's configuration page at
     Administration » Configuration » Content authoring » Suggest.

   - Add suggestions

     Users in roles with the "Add suggestions" permission will see the
     "Contribute" tab when viewing nodes.

   - View suggestions

     Users in roles with the "View suggestions" permission will see all
     contributions submitted to a particular node via the node edit page.

 * Customize the module settings in Administration » Configuration » Content 
   authoring » Suggest.

 * To add Honeypot protection, enable the Honeypot module and check the setting
   on the Suggest module configuration page.


CUSTOMIZATION
-------------

Other modules may alter the suggestable text before it is presented to the user
by implementing hook_suggest_text_alter(). See suggest.api.php for more deatils.


TROUBLESHOOTING
---------------

 * If the "Contribute" tab does not display, check the following:

   - Is the "Add suggestions" permission enabled for the appropriate roles?

   - Does page.tpl.php of your theme output the $tabs variable?

MAINTAINERS
-----------

Current maintainers:
 * Rasmus Werling (Rade) - https://www.drupal.org/user/604700

This project has been supported by:
 * Yle - Finnish Broadcasting Company
   Visit http://yle.fi for more information.

* Druid
  Visit https://druid.fi/en for more information.
