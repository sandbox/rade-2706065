<?php

/**
 * @file
 * Administration options for the Suggest module.
 */

/**
 * Configuration form.
 */
function suggest_admin_form($form, &$form_state) {

  $form['suggest_enabled_nids'] = array(
    '#type' => 'textfield',
    '#title' => t('Enable only for specific nodes'),
    '#description' => t('Comma-separated list of node nids. Leave empty to disable restriction.'),
    '#default_value' => variable_get('suggest_enabled_nids'),
  );

  if (module_exists('honeypot')) {
    $form['suggest_honeypot_enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Honeypot for suggestion forms'),
      '#description' => t('If checked, Honeypot protection will be enabled for the forms provided by this module.'),
      '#default_value' => variable_get('suggest_honeypot_enabled'),
    );
  }

  return system_settings_form($form);
}
