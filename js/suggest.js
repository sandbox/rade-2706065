(function ($) {
  'use strict';
  Drupal.behaviors.suggest = {
    attach: function (context, settings) {
      $('.body-diff, .title-diff').prettyTextDiff({
        // Additional options here.
      });
    }
  };
}(jQuery));
