<?php

/**
 * @file
 * Forms for the Suggest module.
 */

/**
 * Callback for Suggestion form.
 */
function suggest_suggestion_form($form, &$form_state, $node) {
  $emw = entity_metadata_wrapper('node', $node);
  $title = $emw->label();
  $body_field = $emw->body;

  // Get the first value from a multi-value field.
  if ($body_field instanceof EntityListWrapper) {
    $body = $body_field[0]->value->raw();
  }
  // Get the value from a single-value field.
  else {
    $body = $body_field->value->raw();
  }

  suggest_text_alter($body);

  $fields['title']['suggestion'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => t('Suggested title'),
    '#description' => t('Suggest a change to the content title.'),
    '#default_value' => $title,
  );

  $fields['body']['suggestion'] = array(
    '#type' => 'textarea',
    '#required' => TRUE,
    '#title' => t('Suggested body text'),
    '#description' => t('Suggest a change to the content body text.'),
    '#cols' => 50,
    '#rows' => 10,
    '#default_value' => $body,
  );

  $fields['title']['original'] = array(
    '#type' => 'hidden',
    '#value' => $title,
  );

  $fields['body']['original'] = array(
    '#type' => 'hidden',
    '#value' => $body,
  );

  $form['fields'] = $fields;

  $form['reason'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#title' => t('Reason'),
    '#description' => t('Explain why this change should be applied.'),
    '#options' => suggest_get_reason(),
  );

  $user['name'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Your name'),
  );

  $user['contact'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#title' => t('Your contact info (e.g. e-mail or phone number)'),
  );

  $form['user'] = $user;

  $suggestion = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t('Add contribution'),
    '#description' => t('Contribute to article %title.', array('%title' => $emw->label())),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    'children' => $form,
  );

  $form = array('suggestion' => $suggestion);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  if (module_exists('honeypot') && variable_get('suggest_honeypot_enabled')) {
    honeypot_add_form_protection($form, $form_state, array('honeypot', 'time_restriction'));
  }

  return $form;
}

/**
 * Submit callback for Suggestion form.
 */
function suggest_suggestion_form_submit(&$form, &$form_state) {
  $node = $form_state['build_info']['args'][0];
  $suggestion = $form_state['values']['suggestion']['children'];
  $suggestion['entity_type'] = 'node';
  $suggestion['entity_id'] = $node->nid;

  if (suggest_save($suggestion)) {
    drupal_set_message(t('Thank you for your contribution.'));
    drupal_goto('node/' . $node->nid);
  }
  else {
    drupal_set_message(t('Could not save your contribution.'), 'error');
  }
}
